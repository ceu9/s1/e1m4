<?php
      require_once "php/init.php";
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Jerome's Milktea Shop</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css"  href="vendor/css/bootstrap.min.css">
    <link href="vendor/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  href="resource/css/styles.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" 
          rel="stylesheet">
  </head>
  <body data-spy="scroll" data-target=".navbar">

    <header id="review-home">
        <div class="container-fluid navcon">
          <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light pt-md-2 text-danger">
              <div class="container-fluid">
                <span class="navbar-brand mb-0 h1">Milktea Reviews:</span>
                </div>
              </div>
            </nav>
          </div>
        </div>

        <div class="container">
          <div class="jumbotron jumbotron-fluid header-text">
            <div class="container">
            <h1 class="ftreviews pt-md-3">Reviews:</h2>
             <hr>
              <div class="container reviews">
                <form action="" method="GET">
                  <?php listener()?>
                </form>
              </div>
            <?php viewTableData()?>
            <div class="col-md form-group">
              <a class="btn btn-success" type="submit" value="Go to Reviews" href="index.php">Go Back</a>
            </div>
            </div>
          </div>
        </div>
      </header>

          <div class="ftrow">
            <div class="col-md">
              <p class="ft2">
                Jerome's Milktea Shop © 2021. All Rights Reserved&nbsp;&nbsp;&nbsp;
                <i class="fab fa-facebook-square"></i>                  
                <i class="fab fa-twitter"></i>
                <i class="fab fa-instagram"></i>
              </p>
            </div>
          </div>

    <script src="vendor/js/jquery.js"></script>
    <script src="vendor/js/popper.js"></script>
    <script src="vendor/js/bootstrap.bundle.min.js"></script>
    <script src="resource/js/script.js"></script>
  </body>
</html>