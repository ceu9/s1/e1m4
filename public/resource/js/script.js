window.addEventListener("resize", ResizeWindow);
function ResizeWindow(){
    var dataSpyList = [].slice.call(document.querySelectorAll('[data-spy="scroll"]'));
    dataSpyList.array.forEach(function(dataSpyElement) {
        bootstrap.scrollSpy.getInstance(dataSpyElement).refresh();
    });
}

const tx = document.getElementsByTagName("textarea");
for (let i = 0; i < tx.length; i++) {
  tx[i].setAttribute("style", "height:" + (tx[i].scrollHeight) + "px;overflow-y:hidden;");
  tx[i].addEventListener("input", OnInput, false);
}

function OnInput() {
  this.style.height = "auto";
  this.style.height = (this.scrollHeight) + "px";
}