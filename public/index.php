<?php
      require_once "php/init.php";
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Jerome's Milktea Shop</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css"  href="vendor/css/bootstrap.min.css">
    <link href="vendor/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"  href="resource/css/styles.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,  900;1,100;1,300;1,400;1,700;1,900&display=swap" 
          rel="stylesheet">
  </head>
  <body data-spy="scroll" data-target=".navbar">

    <header id="home">
        <div class="container-fluid navcon">
          <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light pt-md-2 text-danger">
              <div class="container-fluid">
                <span class="navbar-brand mb-0 h1">Jerome's Milktea Shop:</span>
                <button class="navbar-toggler" type="button" data-toggle="collapse" 
                        data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" 
                        aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup" data-spy="scroll" data-target=".navbar" data-offset="10">
                  <div class="navbar-nav ml-auto">
                    <a class="nav-link navitem" href="#home">Home</a>
                    <a class="nav-link navitem" href="#gallery">Gallery</a>      
                    <a class="nav-link navitem" href="#aboutus">About Us</a> 
                    <a class="nav-link navitem" href="#reviews">Reviews</a>     
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </div>

        <div class="container">
          <div class="jumbotron jumbotron-fluid header-text">
            <div class="container">
              <h1 class="headermain">Everyone deserves a break, especially for you</h1>
              <a href="#" class="btn btn-ghost">Order Now</a>
              <a href="#" class="btn btn-ghost">Explore the Shop</a>
            </div>
          </div>
        </div>
      </header>

        <section class="section1" id="gallery">
          <div class="container">
            <h2 class="gallerypics pt-md-5">Gallery:</h2>
            <hr>
            <div class="row">
              <div class="col-md-4">
                <div class="card">
                  <img src="resource/img/gallery-1.jpg" class="card-img-top" alt="Ube Milk Tea">
                  <h4>Ube Milk Tea</h4>
                  <div class="card-body">
                    <p class="card-text text-justify">
                      Ube milk tea is a creamy, refreshing and indulgent drink, that is perfect for summer! 
                      Made even more impressive by the vibrant purple color.
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card">
                  <img src="resource/img/gallery-2.jpg" class="card-img-top" alt="Brown Sugar Milk Tea">
                  <h4>Brown Sugar Milk Tea</h4>
                  <div class="card-body">
                    <p class="card-text text-justify">
                      Originating in Taiwan, boba is a refreshing sweet tea beverage with dairy and chewy tapioca pearls, 
                      whose flavors range from coconut and taro to fruit-forward ones infused with passionfruit or peach.
                    </p>  
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card">
                  <img src="resource/img/gallery-3.jpg" class="card-img-top" alt="Strawberry Milk Tea">
                  <h4>Strawberry Milk Tea</h4>
                  <div class="card-body">
                    <p class="card-text text-justify">
                      This Strawberry Milk Tea recipe combines strong black tea, strawberry milk, 
                      and tapioca pearls to form an eye-catching and delicious drink.
                    </p>  
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card">
                  <img src="resource/img/gallery-4.jpg" class="card-img-top" alt="Blue Lagoon Milk Tea">
                  <h4>Blue Lagoon Milk Tea</h4>
                  <div class="card-body">
                    <p class="card-text text-justify">
                      The caffeine free Blue Lagoon Herbal Tea blend from English Tea Store is an exotically flavored herbal tea. 
                      The delicious blend of caramel and fruit medley flavors combined with the natural flavors and tartness
                      of the various dried fruits create a summertime cooler or a wintertime summer reminder.
                    </p>  
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card">
                  <img src="resource/img/gallery-5.jpg" class="card-img-top" alt="Matcha Mlik Tea">
                  <h4>Matcha Milk Tea</h4>
                  <div class="card-body">
                    <p class="card-text text-justify">
                      Matcha milk tea is rejuvenating and refreshing as it is packed with nutrients and health benefits. 
                      This tea makes for a perfect nutritious drink, thanks to its ingredients consisting of simple matcha powder, 
                      milk, and tapioca pearls (if you like the look and feel of those mushy beads).
                    </p>  
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card">
                  <img src="resource/img/gallery-6.jpg" class="card-img-top" alt="Lychee Milk Tea">
                  <h4>Lychee Milk Tea</h4>
                  <div class="card-body">
                    <p class="card-text text-justify">
                      A creamy tropical fruit, with a sweet flavor of citrus and a tinge of rose water. 
                      Combined with Fresh Jasmine Green Tea. It's a balance of sweet and tart. 
                      Lychees are a solid addition to most drinks, and are definitely a bold flavor that, if used properly, 
                      can add to a dish. 
                    </p>  
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </section>
        
        <section class="section2" id="aboutus">
          <div class="container">
            <h2 class="staffmembers pt-md-5">About Us:</h2>
          <hr>

          <div class="row SMCard text-center text-md-left mt-3">
            <div class="col-md-2">
              <img src="resource/img/customer-1.jpg" class="img-fluid" alt="teammember">
            </div>
            <div class="col card-detail">
              <h5 class="SMC-name p-2 p-md-0">John Pierre Polnareff</h5>
              <p class="SMC-p text-justify p-2 p-md-0">
                The mastermind and the expert behind on every milk tea creations. He was originally a bartender
                in France, Italy. But went on a trip through different nations to understand different types of milk tea
                made by the locals until he decided to experiment them to find a perfect concoction loved by
                both locals and foreigners alike.
              </p>
            </div>
          </div>

          <div class="row SMCard text-center text-md-left mt-3">
            <div class="col-md-2">
              <img src="resource/img/customer-2.jpg" class="img-fluid" alt="teammember">
            </div>
            <div class="col card-detail">
              <h5 class="SMC-name p-2 p-md-0">Ermes Costello</h5>
              <p class="SMC-p text-justify p-2 p-md-0">
                A colleague of Polnareff, and an interior designer for 6 years. She is responsible for creating 
                a neat looking shop which is like an ordinary milk tea shop for the many, but an eye-opener for the few.
                It is based on her expertise in both modern and antique art of different centuries made by different
                artists, painters, artisans, and the like, which would mix them together into a harmonious patterns
                of both old and new. 
              </p>
            </div>
          </div>

          <div class="row SMCard text-center text-md-left mt-3">
            <div class="col-md-2">
              <img src="resource/img/customer-3.jpg" class="img-fluid" alt="teammember">
            </div>
            <div class="col card-detail">
              <h5 class="SMC-name p-2 p-md-0">Jonathan Joestar</h5>
              <p class="SMC-p text-justify p-2 p-md-0">
                A gentleman in London, U.K.. As raised by his humble family to become a butler for 10 years. His sincere and
                gentle manners inside the milk tea shop became an attraction towards any novice butlers to know on how to 
                serve properly towards his customers. His was been acquainted numerous times by world-class leaders, royal 
                families, and known actors/actresses, to show off his skills to the world as a genuine butler. 
              </p>
            </div>
          </div>

          <div class="row SMCard text-center text-md-left mt-3">
            <div class="col-md-2">
              <img src="resource/img/customer-4.jpeg" class="img-fluid" alt="teammember">
            </div>
            <div class="col card-detail">
              <h5 class="SMC-name p-2 p-md-0">Anthonio Zedd Zeppeli</h5>
              <p class="SMC-p text-justify p-2 p-md-0">
                A web developer in America for 7 years. He was once a web freelancer in different start-ups in the U.S. to land a
                better, and stable job for the next couple of years but to no avail. He instead flew towards U.K. to find a job there.
                Luckily, he found John Pierre Polnareff, who he gave him a letter of recommendation for a job application as a web developer/designer
                to assist him in building his start-up shop.
              </p>
            </div>
          </div>

          <div class="row SMCard text-center text-md-left mt-3">
            <div class="col-md-2">
              <img src="resource/img/customer-5.jpeg" class="img-fluid" alt="teammember">
            </div>
            <div class="col card-detail">
              <h5 class="SMC-name p-2 p-md-0">Elizabeth Soberanya</h5>
              <p class="SMC-p text-justify p-2 p-md-0">
                A senior saleswoman at a known mall in Hanoi, Vietnam for 6 years. She is an expert in making sales by cooperating with other workers
                within the company by her interpersonal skills and creativity in various marketing strategies, in order to boost the company's product/service
                for better profit on both the consumers, and to them. Recently, she's started working as a cashier in this milk tea shop, 
                which it could take time before she hones another skill.
              </p>
            </div>
          </div>

          </div>
        </section>

        <footer id="reviews">
          <div class="container">
            <h2 class="ftreviews pt-md-5">Reviews:</h2>
             <hr>
              <div class="container reviews">
                <form action="" method="GET">
                  <?php listener()?>
                  <div class="review-class">
                    <div class="form-group">
                        <textarea class="form-control" text="text" name="name" placeholder="Type your name here." required></textarea> 
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" text="text" name="review" placeholder="Type your review here." required></textarea> 
                    </div>
                    <div class="form-group text-center">
                       <input class="btn btn-success" type="submit" value="Send Review"/>
                   </div>
                 </div>
                </form>
              </div>
            <?php viewApprovedData()?>
            <div class="button form-group">
              <a class="btn btn-success" type="submit" value="Go to Reviews" href="review.php">Go to Reviews</a>
            </div>
          </div>
        </footer>

         <div class="ftrow">
            <div class="col-md">
              <p class="ft2">
                Jerome's Milktea Shop © 2021. All Rights Reserved&nbsp;&nbsp;&nbsp;
                <i class="fab fa-facebook-square"></i>                  
                <i class="fab fa-twitter"></i>
                <i class="fab fa-instagram"></i>
              </p>
            </div>
          </div>

    <script src="vendor/js/jquery.js"></script>
    <script src="vendor/js/popper.js"></script>
    <script src="vendor/js/bootstrap.bundle.min.js"></script>
    <script src="resource/js/script.js"></script>
  </body>
</html>