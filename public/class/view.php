<?php
require_once 'config.php';

    class view extends config{
        
        public function viewData(){
            $con = $this->con();
            $sql = "SELECT * FROM `tbl_review` WHERE `status` = 'PENDING'";
            $data = $con -> prepare($sql);
            $data -> execute();
            $result = $data -> fetchAll(PDO::FETCH_ASSOC);
            
            echo "<h3 class='text-dark text-center'> Pending Reviews:</h3>";
            echo "<table class = 'table table-dark table-bordered table-sm table-responsive-sm'>";
            echo "<thead>
                    <th>Name:</th>
                    <th>Time Added:</th>
                    <th>Review:</th>
                    <th>Action:</th>
                    </thead>";
            echo "<tbody>";
            foreach($result as $data){
                echo "<tr>
                      <td>
                        <textarea readonly>
                            $data[name]
                        </textarea>
                      </td>
                      <td>$data[date_added]</td>
                      <td>
                        <textarea readonly>
                            $data[review_statement]
                        </textarea>
                      </td>
                      <td>
                      <a class='btn btn-info' href='index.php?edit=$data[id]'>Approve Review</a>
                      <a class='btn btn-danger' href='index.php?delete=$data[id]'>Reject Review</a>
                      </td>
                      </tr>";
            }
            echo "</tbody>";
            echo "</table>";
        }

        public function viewApprovedData(){
            $con = $this->con();
            $sql = "SELECT * FROM `tbl_review` WHERE `status` = 'APPROVED'";
            $data = $con -> prepare($sql);
            $data -> execute();
            $result = $data -> fetchAll(PDO::FETCH_ASSOC);
            
            echo "<h3 class='text-dark text-center'> Approved Reviews:</h3>";
            echo "<table class = 'table table-striped table-sm table-responsive-sm'>";
            echo "<thead>
                    <th>Name:</th>
                    <th>Time Added:</th>
                    <th>Time Approved:</th>
                    <th>Review:</th>
                    </thead>";
            echo "<tbody>";
            foreach($result as $data){
                echo "<tr>
                      <td>
                        <textarea readonly>
                            $data[name]
                        </textarea>
                      </td>
                      <td>$data[date_added]</td>
                      <td>$data[date_approved]</td>
                      <td>
                        <textarea readonly>
                            $data[review_statement]
                        </textarea>
                      </td>
                      </tr>";
            }
            echo "</tbody>";
            echo "</table>";
        }

    }
?>