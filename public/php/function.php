<?php
    function insertTodo(){
        if(!empty($_GET['name']) && !empty($_GET['review'])){
            $insert = new insert($_GET['name'],$_GET['review']);
            if($insert->insertTask()){
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Insert Review Successful!</strong> Thank you for your response.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>';
            }else{
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Insert Failed!</strong> Something went wrong, sorry.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            }
        }
    }

    function viewTableData(){
        $view = new view;
        $view -> viewData();        
    }

    function viewApprovedData(){
        $view = new view;
        $view -> viewApprovedData();
    }

    function deleteTodo(){
        if(!empty($_GET['delete'])){
            $delete = new delete($_GET['delete']);
            if($delete->deleteTask()){
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Delete Successful!</strong> Sorry to hear that.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            }else{
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Insert Failed!</strong> Something went wrong, sorry.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            }
        }
    }

    function editTodo(){
        if(!empty($_GET['edit'])){
            $edit = new edit($_GET['edit']);
            if($edit->editTask()){
                echo '<div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Review has been approved!</strong> Thank you! We hope to see you soon!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            }else{
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Update Failed!</strong> Something went wrong, sorry.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>';
            }
        }
    }

    function listener(){
        insertTodo();
        deleteTodo();
        editTodo();
    }
?>